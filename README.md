# Binder Requirements

This repository contains definitions that allows to load external projects on ![mybinder.org](https://mybinder.org/static/logo.svg){height=16px}. You have to pipe the reference to the git repository that contains the notebooks you want to work with through your binder link, e.g., 

```
https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.lrz.de%2Ftum-lmf%2Fcourses%2Fenv%2Fbinder/main?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.lrz.de%252Ftum-lmf%252Fcourses%252FCVML2%252FCV2%252FCV2-ST-24%26urlpath%3Dlab%252Ftree%252FCV2-ST-24%252F00%252Findex.ipynb%26branch%3Dmain
```

